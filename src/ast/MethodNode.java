package ast;

import java.util.ArrayList;

import lib.FOOLlib;
import lib.TypeException;

public class MethodNode implements DecNode {

    private final String id;
    private String label;
    private Node exp, symType, type = null;
    private int offset;
    private ArrayList<Node> parlist = new ArrayList<>(); // Lista di parametri
    private ArrayList<Node> declist = new ArrayList<>(); // Lista di dichiarazioni

    public MethodNode(String id, Node type) {
        this.id = id;
        this.type = type;
    }

    public MethodNode(String id, Node type, String label, Node exp, Node symType, int offset, ArrayList<Node> parlist,
            ArrayList<Node> declist) {
        this(id, type);
        this.label = label;
        this.exp = exp;
        this.symType = symType;
        this.offset = offset;
        this.parlist = parlist;
        this.declist = declist;
    }

    public void addDec(Node d) {
        declist.add(d);
    }

    public void addBody(Node b) {
        exp = b;
    }

    public void addPar(Node p) {
        parlist.add(p);
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return offset;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toPrint(String s) {
        String parlstr = "";
        for (final Node par : parlist) {
            parlstr += par.toPrint(s + "  ");
        }
        String declstr = "";
        for (final Node dec : declist) {
            declstr += dec.toPrint(s + "  ");
        }
        return s + "Method:" + id + "\n" + type.toPrint(s + "  ") + parlstr + declstr + exp.toPrint(s + "  ");
    }

    @Override
    public Node typeCheck() throws TypeException {
        for (final Node dec : declist) {
            try {
                dec.typeCheck();
            } catch (final TypeException e) {
                System.out.println("Type checking error in a declaration: " + e.text);
            }
        }
        if (!FOOLlib.isSubtype(exp.typeCheck(), type)) {
            throw new TypeException("Wrong return type for method " + id);
        }
        return null;
    }

    @Override
    public String codeGeneration() {
        String declCode = "", popDecl = "", popParl = "";
        /*
         * In caso tra i parametri o le dichiarazioni vi siano ID di tipo funzionale si
         * devono deallocare due cose dallo stack, quindi facciamo un pop aggiuntivo.
         */
        for (final Node dec : declist) {
            declCode += dec.codeGeneration();
            popDecl += "pop\n";
            if (((DecNode) dec).getSymType() instanceof ArrowTypeNode) {
                popDecl += "pop\n";
            }
        }
        for (final Node par : parlist) {
            popParl += "pop\n";
            if (((DecNode) par).getSymType() instanceof ArrowTypeNode) {
                popParl += "pop\n";
            }
        }

        label = FOOLlib.freshFunLabel();
        FOOLlib.putCode(
				label + ":\n" +
				"cfp\n" + // Mette nel Frame Pointer il contenuto dello Stack Pointer
				"lra\n" + // Mette in cima allo stack il valore di ritorno
				declCode + // Genero il codice delle dichiarazioni
				exp.codeGeneration() + 	// Genero il codice del corpo del metodo
				"stm\n" + // Viene memorizzato in tm il risultato 
				popDecl + // Rimozione dichiarazioni locali dallo stack
				"sra\n" + // Viene memorizzato in RA il risultato
				"pop\n" + // Rimozione AL dallo stack
				popParl + // Rimozione parametri dallo stack
				"sfp\n" + // Viene memorizzato in fp il Control Link
				"ltm\n" + // Metto il risultato del metodo in cima allo stack
				"lra\n" + // Metto in cima allo stack l'indirizzo a cui tornare
				"js\n" // Ritorno da dove sono venuto
		);
        return "";
    }

    @Override
    public Node getSymType() {
        return symType;
    }

    public void setSymType(Node t) {
        if (symType == null) {
            symType = t;
        }
    }

    public String getId() {
        return id;
    }
    
    public Node getType() {
        return type;
    }

    public Node getBody() {
        return exp;
    }

    public ArrayList<Node> getParlist() {
        return parlist;
    }

    public ArrayList<Node> getDeclist() {
        return declist;
    }

}