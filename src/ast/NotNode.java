package ast;

import lib.*;

public class NotNode implements Node {

	private final Node val;

	public NotNode(Node n) {
		val = n;
	}

	@Override
	public String toPrint(String s) {
		return s + "Not\n" + val.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		final Node n = val.typeCheck();
		if (!FOOLlib.isSubtype(n, new BoolTypeNode())) {
			throw new TypeException("Incompatible type in not");
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		final String l1 = FOOLlib.freshLabel();
		final String l2 = FOOLlib.freshLabel();
		return val.codeGeneration() + "push 0\n" + "beq " + l1 + "\n" + "push 0\n" + "b " + l2 + "\n" + l1 + ": \n"
				+ "push 1\n" + l2 + ": \n";
	}
}