package ast;

import lib.TypeException;

public class RefTypeNode implements Node {
	
	private final String id;
	
	public RefTypeNode(String id) {
		this.id = id;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "RefType:" + id + "\n";
	}

	// Inutilizzato
	@Override
	public Node typeCheck() throws TypeException {
		return null;
	}

	// Inutilizzato
	@Override
	public String codeGeneration() {
		return "";
	}
	
	public String getId() {
		return id;
	}

}
