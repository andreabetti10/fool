package ast;

public class STentry {

	private final int nl;
	private Node type = null;
	private final int offset;
	private boolean isMethod;

	public STentry(int n, Node t, int o, boolean m) {
		nl = n;
		type = t;
		offset = o;
		isMethod = m;
	}

	public STentry(int n, Node t, int o) {
		this(n, t, o, false);
	}
	
	public STentry(int n, int o, boolean m) {
            this(n, null, o, m);
        }

	public STentry(int n, int o) {
		this(n, null, o, false);
	}

	public Node getType() {
		return type;
	}

	public void setType(Node t) {
		type = t;
	}

	public int getOffset() {
		return offset;
	}

	public int getNestingLevel() {
		return nl;
	}

	public boolean isMethod() {
		return isMethod;
	}

	public String toPrint(String s) {
		return s + "STentry: nestlev " + nl + "\n" + s + "STentry: type\n " + type.toPrint(s + "  ") + s
				+ "STentry: offset " + offset + "\n" + s + "STentry: isMethod " + isMethod + "\n";
	}

}