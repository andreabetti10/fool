package ast;

import lib.*;

public class IdNode implements Node {

	private String id;
	private int nestingLevel;
	private STentry entry;

	public IdNode(String i, STentry st, int nl) {
		id = i;
		nestingLevel = nl;
		entry = st;
	}

	public String toPrint(String s) {
		return s + "Id:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(s + "  ");
	}

	public Node typeCheck() throws TypeException {
		// ID pu� essere di tipo funzionale (estensione ho) e non deve essere il nome di una classe (cio� di tipo ClassTypeNode) o di un metodo
		if (entry.getType() instanceof ClassTypeNode || entry.isMethod()) {
		    throw new TypeException("Wrong usage of function identifier " + id);
		}
		return entry.getType();
	}

	public String codeGeneration() {
		String getAR = "";
		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++)
			getAR += "lw\n";
		String generatedCode =
				"lfp\n" + // Metto sullo stack il Frame Pointer dell'AR corrente
				getAR + // Risalgo catena di AL per ottenere l'indirizzo dell'AR che contiene la dichiarazione dell'id
				"push " + entry.getOffset() + "\n" +
				"add\n" +
				"lw\n"; // Metto in cima allo stack l'indirizzo della dichiarazione
		
		if (!(entry.getType() instanceof ArrowTypeNode))
			return generatedCode;
		else {
			/* Siccome devo anche gestire l'indirizzo del codice della funzione l'offset occupato � doppio.
			 * [Offset in Symbol Table] Dichiarazione funzione
			 * [Offset in Symbol Table - 1] Indirizzo funzione
			 */
			return generatedCode +
					"lfp\n" + // Metto sullo stack il Frame Pointer dell'AR corrente
					getAR + // Recupero indirizzo AR che contiene l'indirizzo della funzione
					"push " + (entry.getOffset() - 1) + "\n" +
					// Aggiungo l'offset all'indirizzo
					"add\n" +
					"lw\n"; // Metto in cima allo stack l'indirizzo della funzione
		}
	}

	public String getName() {
		return id;
	}

}