package ast;

import lib.*;
import java.util.ArrayList;

public class ProgLetInNode implements Node {

        private ArrayList<Node> cllist = new ArrayList<Node>();
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node exp;

	public ProgLetInNode(ArrayList<Node> c, ArrayList<Node> d, Node e) {
            cllist = c;
	    declist = d;
            exp = e;
        }

	public String toPrint(String s) {
	        String clstr = "";
		String declstr = "";
                for (Node cl : cllist)
                        clstr += cl.toPrint(s + "  ");
		for (Node dec : declist)
			declstr += dec.toPrint(s + "  ");
		return s + "ProgLetIn\n" + clstr + declstr + exp.toPrint(s + "  ");
	}

	public Node typeCheck() throws TypeException {
	        for (Node cl : cllist) {
                        try {
                            cl.typeCheck();
                        } catch (TypeException e) {
                                System.out.println("Type checking error in a class declaration: " + e.text);
                        }
                }
		for (Node dec : declist) {
        		try {
        		    dec.typeCheck();
        		} catch (TypeException e) {
        			System.out.println("Type checking error in a declaration: " + e.text);
        		}
		}
		return exp.typeCheck();
	}

	public String codeGeneration() {
	        String clCode = "";
		String declCode = "";
                for (Node cl : cllist)
                        clCode += cl.codeGeneration();
		for (Node dec : declist)
			declCode += dec.codeGeneration();
		return "push 0\n" + clCode + declCode + exp.codeGeneration() + "halt\n" + FOOLlib.getCode();
	}
}