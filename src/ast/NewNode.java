package ast;

import java.util.List;

import lib.FOOLlib;
import lib.TypeException;

public class NewNode implements Node {

	private final String idClassName;
	private final STentry entry;
	private final List<Node> arguments;

	public NewNode(String id, STentry entry, List<Node> arguments) {
		this.idClassName = id;
		this.entry = entry;
		this.arguments = arguments;
	}

	@Override
	public String toPrint(String indent) {
		String argstr = "";
		for (final Node arg : arguments) {
			argstr += arg.toPrint(indent + "  ");
		}
	    return indent + "New: " + idClassName + "\n" + argstr;
	}

	@Override
	public Node typeCheck() throws TypeException {
		// Check del tipo della creazione (deve essere un ClassTypeNode)
		if (!(entry.getType() instanceof ClassTypeNode)) {
			throw new TypeException("Creation of a non-object " + idClassName);
		}
		ClassTypeNode classTypeNode = (ClassTypeNode) entry.getType();
		// Prendiamo la lista degli argomenti che vuole in input
		List<FieldNode> fields = classTypeNode.getFields();
		// Controllo che gli argomenti passati siano del numero giusto
		if (!(fields.size() == arguments.size())) {
			throw new TypeException("Wrong number of arguments in the creation of an object of type " + idClassName);
		}
		// Controlliamo che siano del tipo corretto
		for (int i = 0; i < arguments.size(); i++) {
			if (!FOOLlib.isSubtype(arguments.get(i).typeCheck(), fields.get(i).getSymType())) {
				throw new TypeException("Wrong type for argument " + (i + 1) + " in the creation of an object of type " + idClassName);
			}
		}
		return new RefTypeNode(idClassName);
	}

	@Override
	public String codeGeneration() {
		String code = "";

		// codeGeneration per i parametri in ordine di apparizione (vengono messi i rispettivi valori sullo stack)
		for (int i = arguments.size() - 1; i >= 0; i--) {
			code+=
					arguments.get(i).codeGeneration() + // Carico codice del parametro sullo stack		
					"lhp\n" + // Carico heap pointer sullo stack	
					"sw\n" + // Poppo heap pointer e carico il parametro nello heap
					"lhp\n" + 
					"push 1\n" +
					"add\n" +
					"shp\n"; // Incremento heap pointer
		}

		return code + 
				"push " + FOOLlib.MEMSIZE + "\n" +
				"push " + entry.getOffset() + "\n" +
				"add\n" + // Ottengo dispatch pointer 
				"lhp\n" + // Carico heap pointer sullo stack
				"sw\n" + // Poppo heap pointer e carico il dispatch pointer nello heap
				"lhp\n" + // Carico object pointer
				"lhp\n" +
				"push 1\n" +
				"add\n" +
				"shp\n"; // Incremento heap pointer

	}

}
