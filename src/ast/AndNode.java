package ast;

import lib.*;

public class AndNode implements Node {

	private final Node left;
	private final Node right;

	public AndNode(Node l, Node r) {
		left = l;
		right = r;
	}

	@Override
	public String toPrint(String s) {
		return s + "And\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		final Node l = left.typeCheck();
		final Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, new BoolTypeNode()) && FOOLlib.isSubtype(r, new BoolTypeNode()))) {
			throw new TypeException("Incompatible types in and");
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		return left.codeGeneration() + right.codeGeneration() + "mult\n";

		/*
		 * Altra soluzione: return left.codeGeneration()+ "push 0\n"+
		 * "beq "+l1+"\n"+//== right.codeGeneration()+ "push 0\n"+ "beq "+l1+"\n"+//==
		 * "push 1"+ "b " + l2 +"\n"+//= l1+": \n"+ "push 0\n"+ l2+": \n";
		 */
	}
}