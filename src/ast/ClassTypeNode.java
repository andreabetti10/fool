package ast;

import java.util.ArrayList;
import java.util.List;

public class ClassTypeNode implements Node {

	//private final String name;
	private final List<FieldNode> allFields = new ArrayList<>();
	private final List<MethodNode> allMethods = new ArrayList<>();

	public ClassTypeNode() {
	}

	public List<FieldNode> getFields() {
		return allFields;
	}

	public List<MethodNode> getMethods() {
		return this.allMethods;
	}

	public void addField(FieldNode field, int offset) {
		this.allFields.add(offset, field);
	}

	public void addOverrideField(FieldNode field, int offset) {
		this.allFields.set(offset, field);
	}

	public void addMethod(MethodNode method, int offset) {
		this.allMethods.add(offset, method);
	}

	public void addOverrideMethod(MethodNode meth, int superOffset) {
		this.allMethods.set(superOffset, meth);
	}

	@Override
	public String toPrint(String s) {
		String fstr = "";
		for (final Node f : allFields) {
			fstr += f.toPrint(s + "  ");
		}
		String mstr = "";
		for (final Node m : allMethods) {
			mstr += m.toPrint(s + "  ");
		}
		return s + "ClassTypeNode: " + "\n" + fstr + mstr;
	}

	// non utilizzato
	@Override
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	@Override
	public String codeGeneration() {
		return "";
	}

}