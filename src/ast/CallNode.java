package ast;

import java.util.ArrayList;
import java.util.List;

import lib.*;

public class CallNode implements Node {

	private final String id;
	private final int nestingLevel;
	private final STentry entry;
	private List<Node> parlist = new ArrayList<>();

	public CallNode(String i, STentry st, List<Node> p, int nl) {
		id = i;
		entry = st;
		parlist = p;
		nestingLevel = nl;
	}

	@Override
    public String toPrint(String s) {
		String parlstr = "";
		for (final Node par : parlist) {
            parlstr += par.toPrint(s + "  ");
        }
		return s + "Call:" + id + " at nestinglevel " + nestingLevel + "\n" + entry.toPrint(s + "  ") + parlstr;
	}

	@Override
    public Node typeCheck() throws TypeException {
		// Check del tipo della chiamata (deve essere un ArrowTypeNode)
		if (!(entry.getType() instanceof ArrowTypeNode)) {
            throw new TypeException("Invocation of a non-function " + id);
        }
		final ArrowTypeNode t = (ArrowTypeNode) entry.getType();
		// Prendiamo la lista dei parametri che vuole in input
		final List<Node> p = t.getParList();
		// Controlliamo che siano il numero corretto
		if (!(p.size() == parlist.size())) {
            throw new TypeException("Wrong number of parameters in the invocation of " + id);
        }
		// Controlliamo che siano del tipo corretto
		for (int i = 0; i < parlist.size(); i++) {
            if (!FOOLlib.isSubtype(parlist.get(i).typeCheck(), p.get(i))) {
                throw new TypeException("Wrong type for parameter " + (i + 1) + " in the invocation of " + id);
            }
        }
		return t.getReturnType();
	}

	@Override
    public String codeGeneration() {
		String parCode = "", getAR = "";
		for (int i = parlist.size() - 1; i >= 0; i--) {
            parCode += parlist.get(i).codeGeneration();
        }
		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++) {
            getAR += "lw\n";

            // se entry e' un metodo doppio salto per raggiungere la dispatch table (codice OO)
            if (entry.isMethod()) {
                getAR += "lw\n";
            }
        }
		/* Devo recuperare due valori.
		 * [Offset in Symbol Table] Dichiarazione funzione
		 * [Offset in Symbol Table - 1] Indirizzo funzione
		 */
         return "lfp\n" + // Metto Control Link (Frame Pointer dell'ID della funzione chiamante) in cima allo stack
				parCode + // Genero il codice delle espressioni dei parametri (in ordine inverso)
				"lfp\n" +
				getAR + // Push dell'Access Link (puntatore all'AR della dichiarazione della funzione)
				"push " + entry.getOffset() + "\n" +
				"add\n" + 
				"lw\n" + // Push dell'indirizzo della dichiarazione della funzione (set nuovo AL)
				"lfp\n" + 
				getAR +
				"push " + (entry.getOffset() - 1) + "\n" +
				"add\n" +
				"lw\n" +
				"js\n"; // Salto a codice funzione 
	}

}