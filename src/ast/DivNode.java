package ast;

import lib.*;

public class DivNode implements Node {

	private final Node left;
	private final Node right;

	public DivNode(Node l, Node r) {
		left = l;
		right = r;
	}

	@Override
	public String toPrint(String s) {
		return s + "Div\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode())
				&& FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()))) {
			throw new TypeException("Non integers in div");
		}
		return new IntTypeNode();
	}

	@Override
	public String codeGeneration() {
		return left.codeGeneration() + right.codeGeneration() + "div\n";
	}

}