package ast;

import lib.*;

public class LessOrEqualNode implements Node {

	private final Node left;
	private final Node right;

	public LessOrEqualNode(Node l, Node r) {
		left = l;
		right = r;
	}

	@Override
	public String toPrint(String s) {
		return s + "LessOrEqualNode\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		final Node l = left.typeCheck();
		final Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			throw new TypeException("Incompatible types in equal");
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		final String l1 = FOOLlib.freshLabel();
		final String l2 = FOOLlib.freshLabel();
		return left.codeGeneration() + // equivalente a GreaterOrEqual con operandi invertiti
				right.codeGeneration() +
				"bleq " + l1 + "\n" +
				"push 0\n" +
				"b " + l2 + "\n" +
				l1 + ": \n" +
				"push 1\n" +
				l2 + ": \n";
	}

}