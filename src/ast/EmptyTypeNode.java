package ast;

public class EmptyTypeNode implements Node {

        @Override
        public String toPrint(String s) {
                return s + "NullType\n";
        }

        // non utilizzato
        @Override
        public Node typeCheck() {
                return null;
        }

        // non utilizzato
        @Override
        public String codeGeneration() {
                return "";
        }

}