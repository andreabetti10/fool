package ast;

import ast.EmptyTypeNode;

public class EmptyNode implements Node {

    @Override
    public String toPrint(String s) {
        return s + "Null\n";
    }

    @Override
    public Node typeCheck() {
        return new EmptyTypeNode();
    }

    @Override
    public String codeGeneration() {
        return "push -1\n";
    }

}