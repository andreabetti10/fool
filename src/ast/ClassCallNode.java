package ast;

import java.util.ArrayList;
import java.util.List;

import lib.FOOLlib;
import lib.TypeException;

public class ClassCallNode implements Node {
	
	private final String classId, methodId;
	private final STentry classEntry, methodEntry;
	private List<Node> parlist = new ArrayList<>();
	private int nestingLevel;
	
	public ClassCallNode(String classId, String methodId, STentry classEntry, STentry methodEntry, List<Node> parlist, int nestingLevel) {
		this.classId = classId;
		this.methodId = methodId;
		this.classEntry = classEntry;
		this.methodEntry = methodEntry;
		this.parlist = parlist;
		this.nestingLevel = nestingLevel;
	}

	@Override
	public String toPrint(String s) {
		String parlstr = "";
                for (final Node par : parlist) {
                    parlstr += par.toPrint(s + "  ");
                }
            return s + "ClassCall: " + classId + "." + methodId + " at nestinglevel " + nestingLevel + "\n" + classEntry.toPrint(s + "  ") + methodEntry.toPrint(s + "  ") + parlstr;
	}

	@Override
	public Node typeCheck() throws TypeException {
		final ArrowTypeNode t = (ArrowTypeNode) methodEntry.getType();
		// Prendiamo la lista degli argomenti che vuole in input
		final List<Node> p = t.getParList();
		// Controlliamo che siano il numero corretto
		if (!(p.size() == parlist.size())) {
			throw new TypeException("Wrong number of arguments in the invocation of " + methodId);
		}
		// Controlliamo che siano del tipo corretto
		for (int i = 0; i < parlist.size(); i++) {
			if (!FOOLlib.isSubtype(parlist.get(i).typeCheck(), p.get(i))) {
				throw new TypeException("Wrong type for argument " + (i + 1) + " in the invocation of " + methodId);
			}
		}
		return t.getReturnType();
	}

	@Override
	public String codeGeneration() {
	    String parCode = "", getAR = "";
            for (int i = parlist.size() - 1; i >= 0; i--) {
                parCode += parlist.get(i).codeGeneration();
            }
            for (int i = 0; i < nestingLevel - classEntry.getNestingLevel(); i++) {
                getAR += "lw\n";
            }
            /* Devo recuperare due valori.
             * [Offset in Symbol Table della classe] Object pointer
             * [Offset in Symbol Table del metodo] Indirizzo metodo
             */
            return "lfp\n" + // Metto Control Link (Frame Pointer dell'ID del metodo chiamante) in cima allo stack
                    parCode + // Genero il codice delle espressioni dei parametri (in ordine inverso)
                    "lfp\n" +
                    getAR + // Push dell'Access Link (puntatore all'AR della dichiarazione della classe)
                    "push " + classEntry.getOffset() + "\n" +
                    "add\n" +
                    "lw\n" + // Push dell'object pointer (set nuovo AL)
                    "stm\n" + "ltm\n" + "ltm\n" +
                    "lw\n" + "lw\n" + // Push dell'indirizzo della classe
                    "push " + methodEntry.getOffset() + "\n"+ 
                    "add\n" +
                    "lw\n" +
                    "js\n"; // Salto a codice metodo 
	}

}
