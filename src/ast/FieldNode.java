package ast;

public class FieldNode implements DecNode {

    private final String id;
    private final Node type;
    private int offset;

    public FieldNode(String id, Node type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String toPrint(String s) {
        return s + "Field:" + this.id + "\n" + type.toPrint(s + "  ");
    }

    // non utilizzato
    @Override
    public Node typeCheck() {
        return null;
    }

    // non utilizzato
    @Override
    public String codeGeneration() {
        return "";
    }

    @Override
    public Node getSymType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

}