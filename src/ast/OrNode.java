package ast;

import lib.*;

public class OrNode implements Node {

	private final Node left;
	private final Node right;

	public OrNode(Node l, Node r) {
		left = l;
		right = r;
	}

	@Override
	public String toPrint(String s) {
		return s + "Or\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		final Node l = left.typeCheck();
		final Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, new BoolTypeNode()) && FOOLlib.isSubtype(r, new BoolTypeNode()))) {
			throw new TypeException("Incompatible types in or");
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		final String l1 = FOOLlib.freshLabel();
		final String l2 = FOOLlib.freshLabel();
		return left.codeGeneration() +
				"push 1\n" +
				"beq " + l1 + "\n" +
				right.codeGeneration() +
				"push 1\n" +
				"beq " + l1 + "\n"
				+ "push 0\n" +
				"b " + l2 + "\n" + 
				l1 + ": \n" + 
				"push 1\n" + l2 + ": \n";
	}
}