package ast;

import java.util.ArrayList;
import java.util.List;

import lib.FOOLlib;
import lib.TypeException;

public class ClassNode implements DecNode {

	private final String id;
	private final Node type;
	private Node symType;
	private String superId;
	private STentry superEntry;
	private final List<FieldNode> fields = new ArrayList<>();
	private final List<MethodNode> methods = new ArrayList<>();

	public ClassNode(String id, Node type) {
		this.id = id;
		this.type = type;
	}

	public void addField(FieldNode field, int offset) {
		this.fields.add(offset, field);
	}

	public void addOverrideField(FieldNode field, int offset) {
		this.fields.set(offset, field);
	}

	public void addMethod(MethodNode method, int offset) {
		this.methods.add(offset, method);
	}

	public void addOverrideMethod(MethodNode meth, int superOffset) {
		this.methods.set(superOffset, meth);
	}

	@Override
	public Node getSymType() {
		return symType;
	}

	public void setSymType(Node t) {
		if (symType == null) {
			symType = t;
		}
	}

	public List<FieldNode> getFields() {
		return fields;
	}

	public List<MethodNode> getMethods() {
		return methods;
	}

	public void setSuperId(String superId) { 
	        this.superId = superId;
	}
	
	public void setSuperEntry(STentry superEntry) { 
	        this.superEntry = superEntry;
	}

	@Override
	public String toPrint(String s) {
		String fstr = "";
		for (final Node f : fields) {
			fstr += f.toPrint(s + "  ");
		}
		String mstr = "";
		for (final Node m : methods) {
			mstr += m.toPrint(s + "  ");
		}
		String extendsstr = "";
		if (superId != null) {
		    extendsstr += " extends " + superId;
		}
		return s + "Class:" + id + extendsstr + "\n" + type.toPrint(s + "  ") + fstr + mstr;
	}

	@Override
	public Node typeCheck() throws TypeException {
	    for (final Node m : methods) {
                m.typeCheck();
            }
	    
	    if (superEntry != null) {
	        final ClassTypeNode superType = (ClassTypeNode) superEntry.getType();

	        // Verifica correttezza overriding tipo di ritorno
	        if (!FOOLlib.isSubtype(type, superType)) {
                    throw new TypeException("Class " + id + " not a subtype of the parent class");
                }
	        
	        final List<FieldNode> superFields = superType.getFields();
	        final List<MethodNode> superMethods = superType.getMethods();
	        
	        // Verifica correttezza overriding dei campi
	        for (final FieldNode f : superFields) {
	            final int index = superFields.indexOf(f);
                    final int fieldOffset = -fields.get(index).getOffset() - 1; // Ottimizzazione
	            
                    if (fieldOffset < superFields.size()) { // Primo if ottimizzazione
                        if (!FOOLlib.isSubtype(this.fields.get(index), superFields.get(index))) {
                            throw new TypeException(index + " field " + 1 + " not a subtype of the parent field with the same position");
                        }
                    }
	        }
	        
	        // Verifica correttezza overriding dei metodi
	        for (final MethodNode m : superMethods) {
                    final int index = superMethods.indexOf(m);
                    final int methodOffset = methods.get(index).getOffset(); // Ottimizzazione
                    
                    if (methodOffset < superMethods.size()) { // Primo if ottimizzazione
                        if (!FOOLlib.isSubtype(this.methods.get(index), superMethods.get(index))) {
                            throw new TypeException(index + " field " + 1 + " not a subtype of the parent method with the same position");
                        }
                    }
                }
	    }
		
	    return null;
	}

	@Override
	public String codeGeneration() {
		// Creo dispatch table di questa classe
	        final List<String> dispatchTable;
    	        if (superEntry == null) {
    	            dispatchTable = new ArrayList<>();
    	        } else {
    	            // Eredita dispatch table della eventuale classe padre
    	            dispatchTable = FOOLlib.DISPATCH_TABLES.get(-superEntry.getOffset() - 2);
    	        }

		// Per ogni metodo figlio:
		for (final MethodNode m : methods) {
			// Invoco la sua code generation
			m.codeGeneration();
			// Aggiorno la dispatch table settando la posizione del metodo e la sua
			// etichetta
			dispatchTable.add(m.getOffset(), m.getLabel());
		}

		// Aggiungo la nuova dispatch table sullo heap
		FOOLlib.DISPATCH_TABLES.add(dispatchTable);

		// Per ciascuna etichetta nella dispatch table...
		// (dopo avere lasciato il dispatch pointer sullo stack)
		return dispatchTable.stream().map(label ->
		// La memorizzo nell'indirizzo hp
		"push " + label + "\n" + "lhp\n" + "sw\n" +
		// Incremento hp
				"lhp\n" + "push 1\n" + "add\n" + "shp\n")
		        .reduce("lhp\n", (partial, methodString) -> partial + methodString);

	}
}