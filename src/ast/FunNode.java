package ast;

import java.util.ArrayList;
import lib.*;

public class FunNode implements DecNode {

	private final String id;
	private Node exp, symType, type = null;
	private final ArrayList<Node> parlist = new ArrayList<>(); // Lista di parametri
	private ArrayList<Node> declist = new ArrayList<>(); // Lista di dichiarazioni

	public FunNode(String i, Node t) {
		id = i;
		type = t;
	}
	
	public FunNode(String i) {
		id = i;
	}

	public void addDeclist(ArrayList<Node> d) {
		declist.addAll(d);
	}

	public void addBody(Node b) {
		exp = b;
	}

	public void addPar(Node p) {
		parlist.add(p);
	}

	@Override
	public String toPrint(String s) {
		String parlstr = "";
		for (final Node par : parlist) {
			parlstr += par.toPrint(s + "  ");
		}
		String declstr = "";
		for (final Node dec : declist) {
			declstr += dec.toPrint(s + "  ");
		}
		return s + "Fun:" + id + "\n" + type.toPrint(s + "  ") + parlstr + declstr + exp.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() throws TypeException {
		for (final Node dec : declist) {
			try {
				dec.typeCheck();
			} catch (final TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		}
		if (!FOOLlib.isSubtype(exp.typeCheck(), type)) {
			throw new TypeException("Wrong return type for function " + id);
		}
		return null;
	}

	@Override
	public String codeGeneration() {
		String declCode = "", popDecl = "", popParl = "";
		/*
		 * In caso tra i parametri o le dichiarazioni vi siano ID di tipo funzionale si
		 * devono deallocare due cose dallo stack, quindi facciamo un pop aggiuntivo.
		 */
		for (final Node dec : declist) {
			declCode += dec.codeGeneration();
			popDecl += "pop\n";
			if (((DecNode) dec).getSymType() instanceof ArrowTypeNode) {
				popDecl += "pop\n";
			}
		}
		for (final Node par : parlist) {
			popParl += "pop\n";
			if (((DecNode) par).getSymType() instanceof ArrowTypeNode) {
				popParl += "pop\n";
			}
		}
			
		final String funl = FOOLlib.freshFunLabel();
		FOOLlib.putCode(
				funl + ":\n" +
				"cfp\n" + // Mette nel Frame Pointer il contenuto dello Stack Pointer
				"lra\n" + // Mette in cima allo stack il valore di ritorno
				declCode + // Genero il codice delle dichiarazioni
				exp.codeGeneration() + 	// Genero il codice del corpo della funzione
				"stm\n" + // Viene memorizzato in tm il risultato 
				popDecl + // Rimozione dichiarazioni locali dallo stack
				"sra\n" + // Viene memorizzato in RA il risultato
				"pop\n" + // Rimozione AL dallo stack
				popParl + // Rimozione parametri dallo stack
				"sfp\n" + // Viene memorizzato in fp il Control Link
				"ltm\n" + // Metto il risultato della funzione in cima allo stack
				"lra\n" + // Metto in cima allo stack l'indirizzo a cui tornare
				"js\n" // Ritorno da dove sono venuto
		);
        return "lfp\n" + // Pusho indirizzo dell'AR corrente
		"push " + funl + "\n"; // Pusho indirizzo della funzione
	}

	@Override
	public Node getSymType() {
		return symType;
	}
	
	public void setSymType(Node t) {
		if (symType == null) {
			symType = t;
		}
	}

}