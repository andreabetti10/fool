package lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import ast.*;

public class FOOLlib {

	public static int typeErrors = 0;
	public static final Map<String, String> SUPERTYPE = new HashMap<>();
	public static final int MEMSIZE = 10000;
	public static final List<List<String>> DISPATCH_TABLES = new ArrayList<>();

	/**
	 * Valuta se il tipo "a" e' <= al tipo "b", dove "a" e "b" sono tipi di base:
	 * int o bool
	 */
	public static boolean isSubtype(Node a, Node b) {

		// Per tipi funzionali
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			Node returnA = ((ArrowTypeNode) a).getReturnType();
			Node returnB = ((ArrowTypeNode) b).getReturnType();
			// Controllo se il tipo del valore di ritorno di e' sottotipo di quello di b
			// (covarianza)
			if (!isSubtype(returnA, returnB))
				return false;
			List<Node> parListA = ((ArrowTypeNode) a).getParList();
			List<Node> parListB = ((ArrowTypeNode) b).getParList();
			// Il numero dei parametri di input deve essere lo stesso per entrambi
			// (controvarianza)
			if (parListA.size() != parListB.size())
				return false;
			// Controllo se l'i-esimo tipo del parametro di e' sottotipo del corrispettivo
			// in b (controvarianza)
			for (int i = 0; i < parListA.size(); i++) {
				if (!isSubtype(parListA.get(i), parListB.get(i)))
					return false;
			}
			return true;
		}

		// Per tipo nullo
		if (a instanceof EmptyTypeNode && b instanceof RefTypeNode) {
			return true;
		}

		// Per tipi primitivi
		if (a.getClass().equals(b.getClass()) || ((a instanceof BoolTypeNode) && (b instanceof IntTypeNode))) {
			return true;
		}

		// Per supertipi
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			return isSupertype(a, b);
		}

		return false;

	}

	private static boolean isSupertype(Node a, Node b) {
		RefTypeNode father = (RefTypeNode) a;
		RefTypeNode child = (RefTypeNode) b;
		return isSuperType(father.getId(), child.getId());
	}

	private static boolean isSuperType(String idSuper, String id) {
		boolean isSuperType = false;
		if (SUPERTYPE.containsKey(id)) {
			String idChild = SUPERTYPE.get(id);
			String idFather = SUPERTYPE.get(idSuper);
			if (idChild == idSuper) {
				return true;
			} else {
				isSuperType(idFather, id);
			}
		}
		return isSuperType;
	}

	private static int labCount = 0;

	public static String freshLabel() {
		return "label" + (labCount++);
	}

	private static int funlabCount = 0;

	public static String freshFunLabel() {
		return "function" + (funlabCount++);
	}

	private static String funCode = "";

	public static void putCode(String c) {
		funCode += "\n" + c; // Aggiunge una linea vuota di separazione prima di funzione
	}

	public static String getCode() {
		return funCode;
	}

	public static Node lowestCommonAncestor(Node a, Node b) {
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			final RefTypeNode aRef = (RefTypeNode) a;
			final RefTypeNode bRef = (RefTypeNode) b;
			return getLowestCommonAncestor(aRef.getId(), bRef.getId());
		} else if (a instanceof EmptyTypeNode && !(b instanceof EmptyTypeNode)) {
			return b;
		} else if (!(a instanceof EmptyTypeNode) && b instanceof EmptyTypeNode) {
			return a;
		} else if (a instanceof IntTypeNode || b instanceof IntTypeNode) {
			return new IntTypeNode();
		} else if (a instanceof BoolNode || b instanceof BoolTypeNode) {
			return new BoolTypeNode();
		} else if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			final ArrowTypeNode aFun = (ArrowTypeNode) a;
			final ArrowTypeNode bFun = (ArrowTypeNode) b;
			final List<Node> aFunPar = aFun.getParList();
			final List<Node> bFunPar = bFun.getParList();
			if (aFunPar.size() == bFunPar.size()) {
				final Node lca = lowestCommonAncestor(aFun.getReturnType(), bFun.getReturnType());
				if (lca != null) {
					final List<Node> types = new ArrayList<>();
					IntStream.range(0, aFunPar.size()).forEach(i -> {
						final Node aPar = aFunPar.get(i);
						final Node bPar = bFunPar.get(i);
						if (isSubtype(aPar, bPar))
							types.add(aPar);
						if (isSubtype(bPar, aPar))
							types.add(bPar);
					});
					return new ArrowTypeNode(types, lca);
				}
			}
		}
		return null;
	}

	private static Node getLowestCommonAncestor(String aId, String bId) {
		final RefTypeNode a = new RefTypeNode(aId);
		final RefTypeNode b = new RefTypeNode(bId);
		if (isSubtype(b, a)) {
			return a;
		} else {
			String aSupertype = SUPERTYPE.get(aId);
			return aSupertype != null ? getLowestCommonAncestor(aSupertype, bId) : null;
		}
	}

}