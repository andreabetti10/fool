grammar FOOL;

@header{
	import java.util.*;
	import ast.*;
	import lib.FOOLlib;
}

@parser::members{
	int stErrors = 0;
	private int globalOffset = -2; // Stack parte da 9998 (e non 10000)
	private int nestingLevel = 0;
	/* 
	 * Livello dello scope con dichiarazioni piu esterno e' 0 (prima posizione ArrayList) invece che 1 (slides)
	 * il "fronte" della lista di tabelle e' symTable.get(nestingLevel)
	 */
	private List<Map<String, STentry>> symTable = new ArrayList<Map<String, STentry>>();
	/*
	 * Preserva le dichiarazioni interne ad una classe
	 */
	private Map<String, Map<String, STentry>> classTable = new TreeMap<>();
}

@lexer::members {
	int lexicalErrors = 0;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
 
prog returns [Node ast] :
	{
		HashMap<String, STentry> hm = new HashMap<String, STentry>();
		symTable.add(hm);
	}          
	(
		e = exp
			{
				$ast = new ProgNode($e.ast);
			}
		|
			LET 
			{
				ArrayList<Node> cllist = new ArrayList<Node>();
				ArrayList<Node> declist = new ArrayList<Node>();				
			}
			(c = cllist 
				{
					cllist.addAll($c.classlist);
				}
				(d = declist
					{
						declist.addAll($d.astlist);
					}
				)? 
			| d = declist
				{
					declist.addAll($d.astlist);
				}
			) IN e = exp
			{
				$ast = new ProgLetInNode(cllist, declist, $e.ast);
			}      
	)
	{
		symTable.remove(nestingLevel);
	}
	SEMIC EOF
;

cllist returns [ArrayList<Node> classlist] : 
	{ 
		$classlist = new ArrayList<Node>();
	}
	(
		CLASS id = ID
		{
			int methodOffset = 0;
			int fieldOffset = -1;
			ClassTypeNode type = new ClassTypeNode();
			ClassNode clNode = new ClassNode($id.text, type);
			Map<String, STentry> virtualTable = new HashMap<>();
			Set<String> declarations = new HashSet<>();
		}
		(
			EXTENDS id2 = ID
			{
				/* STentry presa direttamente da livello 0 della Symbol Table */
				STentry superEntry = null;
				superEntry = symTable.get(0).get($id2.text);
				if (superEntry == null) {
					System.out.println("Superclass id " + $id2.text + " at line " + $id2.line + " not declared");
					stErrors++;
				} else {
					clNode.setSuperEntry(superEntry);
					FOOLlib.SUPERTYPE.put($id.text, $id2.text);
				}
				
				/* Vengono copiati i metodi e i campi dalla classe da cui eredita
				 * (non il riferimento)
				 */
				ClassTypeNode superType = (ClassTypeNode) superEntry.getType();
				List<FieldNode> superFields = superType.getFields();
				List<MethodNode> superMethods = superType.getMethods();
				for (FieldNode f : superFields) {
					type.addField(new FieldNode(f.getId(), f.getSymType()), superFields.indexOf(f));
					clNode.addField(new FieldNode(f.getId(), f.getSymType()), superFields.indexOf(f));
				}
				for (MethodNode m : superMethods) {
					type.addMethod(new MethodNode(m.getId(), m.getType(), m.getLabel(), m.getBody(), m.getSymType(), m.getOffset(), (ArrayList<Node>) m.getParlist().clone(), (ArrayList<Node>) m.getDeclist().clone()), superMethods.indexOf(m));
					clNode.addMethod(new MethodNode(m.getId(), m.getType(), m.getLabel(), m.getBody(), m.getSymType(), m.getOffset(), (ArrayList<Node>) m.getParlist().clone(), (ArrayList<Node>) m.getDeclist().clone()), superMethods.indexOf(m));
				}
				clNode.setSuperId($id2.text);
					
				/* Viene copiato il contenuto della virtual table dalla classe da cui eredita
				 * (non il riferimento)
				 */
				Map<String, STentry> superVirtualTable = classTable.get($id2.text);
				if (superVirtualTable == null) {
					System.out.println($id2.text + " must be in Class Table");
					stErrors++;
				}
				
				for (Map.Entry<String, STentry> e : superVirtualTable.entrySet()) {
					STentry value = e.getValue();
					virtualTable.put(e.getKey(), new STentry(value.getNestingLevel(), value.getType(), value.getOffset(), value.isMethod()));
				}
				
				fieldOffset = -type.getFields().size() - 1;
				methodOffset = type.getMethods().size();
			}
		)?
		{
			/* Entry deve essere presa direttamente dal livello 0 */
			STentry entry = new STentry(0, type, globalOffset--, false);
			
			/* nella Symbol Table (livello 0)... */
			Map<String, STentry> hm = symTable.get(0);
			
			/* ...viene aggiunto il nome della classe 
			 * mappato ad una nuova STentry */
			if (hm.put($id.text, entry) != null) {
				System.out.println("Class " + $id.text + " at line " + $id.line + " already declared");
				stErrors++;
			}
			/* se non si eredita, il tipo e' un nuovo oggetto ClassTypeNode
				con una lista inizialmente vuota in allFields e allMethods */
			
			/* nella Class Table viene aggiunto il nome della classe 
			 * mappato ad una nuova Virtual Table
			 */
			classTable.put($id.text, virtualTable);
			/* se non si eredita, la Virtual Table viene creata vuota */
			
			/* viene creato un nuovo livello per la symbol table
			 * con la nuova Virtual Table creata
			 *  (ogni livello e' un riferimento) */
			symTable.add(virtualTable);
			
			nestingLevel++;
		}
		LPAR
			(
				fid = ID COLON ft = type
				{
					/* Virtual Table e oggetto ClassTypeNode (contenuto
						dentro la STentry del nome della classe) vengono
						aggiornati tutte le volte che si incontra
						la dichiarazione di un campo */
					FieldNode fField = new FieldNode($fid.text, $ft.ast);
					
					/* SENZA EREDITARIETA'
					  type.addField(fField, -fieldOffset - 1);
					  clNode.addField(fField, -fieldOffset - 1);
					  
					  STentry fFieldEntry = new STentry(nestingLevel, $ft.ast, fieldOffset--);

						if (virtualTable.put($fid.text, fFieldEntry) != null) {
							System.out.println("Field " + $fid.text + " at line " + $fid.line + " already declared");
							stErrors++;
						}
					*/
					
					/* aggiungo dich a VirtualTable */
					STentry fFieldEntry = null;
					if (virtualTable.containsKey($fid.text)) {
						STentry superEntry = virtualTable.get($fid.text);
							if (superEntry.isMethod()) {
								System.out.println("Field id " + $fid.text + " at line " + $fid.line + " is overriding a method");
		                		stErrors++;
							} else {
								int superOffset = superEntry.getOffset();
								fField.setOffset(superOffset); /* Ottimizzazione */
								fFieldEntry = new STentry(nestingLevel, $ft.ast, superOffset);
								type.addOverrideField(fField, -superOffset - 1);
								clNode.addOverrideField(fField, -superOffset - 1);	
							}
					} else {
						fField.setOffset(fieldOffset);	/* Ottimizzazione */
						type.addField(fField, -fieldOffset - 1);
						clNode.addField(fField, -fieldOffset - 1);
						fFieldEntry = new STentry(nestingLevel, $ft.ast, fieldOffset--);
					}
					
					virtualTable.put($fid.text, fFieldEntry);
				
				}
					(
						COMMA id = ID COLON t = type
						{
							/* FOOL Ottimizzazione */
							if (declarations.contains($id.text)) {
								System.out.println("Field id " + $fid.text + " at line " + $fid.line + " already declared inside class");
								stErrors++;
							}

							FieldNode field = new FieldNode($id.text, $t.ast);

							/*  SENZA EREDITARIETA'
								type.addField(field, -fieldOffset - 1);
								clNode.addField(field, -fieldOffset - 1);
								
								STentry fieldEntry = new STentry(nestingLevel, $t.ast, fieldOffset--);
								
								if (virtualTable.put($id.text, fieldEntry) != null) {
									System.out.println("Field " + $id.text + " at line " + $id.line + " already declared");
									stErrors++;
								}	
							*/
							
							/* aggiungo dich a VirtualTable */
							STentry fieldEntry = null;
						  if (virtualTable.containsKey($id.text)) {
								STentry superEntry = virtualTable.get($id.text);
									if (superEntry.isMethod()) {
										System.out.println("Field id " + $id.text + " at line " + $id.line + " is overriding a method");
				                		stErrors++;
									} else {
										int superOffset = superEntry.getOffset();
										field.setOffset(superOffset); /* Ottimizzazione */
										fieldEntry = new STentry(nestingLevel, $t.ast, superOffset);
										type.addOverrideField(field, -superOffset - 1);
										clNode.addOverrideField(field, -superOffset - 1);
									}
							} else {
								field.setOffset(fieldOffset); /* Ottimizzazione */
								type.addField(field, -fieldOffset - 1);
								clNode.addField(field, -fieldOffset - 1);
								fieldEntry = new STentry(nestingLevel, $t.ast, fieldOffset--);
							}

						virtualTable.put($id.text, fieldEntry);
						}
					)*
			)?
				RPAR
				CLPAR
					(
						FUN fid = ID COLON ft = type
						{
							/* FOOL Ottimizzazione */
							if (declarations.contains($fid.text)) {
								System.out.println("Method id " + $fid.text + " at line " + $fid.line + " already declared inside class");
								stErrors++;
							}
						
							/* Virtual Table e oggetto ClassTypeNode (contenuto
								dentro la STentry del nome della classe) vengono
								aggiornati tutte le volte che si incontra
								la dichiarazione di un metodo */
							List<Node> parameters = new ArrayList<>();
							MethodNode method = new MethodNode($fid.text, $ft.ast);	
						  
						  /* SENZA EREDITARIETA' */
							/*  
							  type.addMethod(method, methodOffset);
							  clNode.addMethod(method, methodOffset);
							  method.setOffset(methodOffset);
							  
							  STentry methodEntry = new STentry(nestingLevel, $ft.ast, methodOffset++);
							 
							  if (virtualTable.put($fid.text, methodEntry) != null) {
									System.out.println("Method " + $fid.text + " at line " + $fid.line + " already declared");
									stErrors++;
								}
							*/
							
							
						/* aggiungo dich a VirtualTable */
						STentry methodEntry = null;
						if (virtualTable.containsKey($fid.text)) {
							STentry superEntry = virtualTable.get($fid.text);
							if (!superEntry.isMethod()) {
								System.out.println("Method id " + $fid.text + " at line " + $fid.line + " is overriding a field");
		                		stErrors++;
							} else {
								int superOffset = superEntry.getOffset();
								method.setOffset(superOffset);
								methodEntry = new STentry(nestingLevel, superOffset, true);
								type.addOverrideMethod(method, superOffset);
								clNode.addOverrideMethod(method, superOffset);
							}
						} else {
								method.setOffset(methodOffset);
								type.addMethod(method, methodOffset);
								clNode.addMethod(method, methodOffset);
								methodEntry = new STentry(nestingLevel, methodOffset++, true);
					 }
					 virtualTable.put($fid.text, methodEntry);
							
							/*
							 * Creo scope aggiuntivo (per il metodo dichiarato)
							 * Incremento nesting level
							 */
							Map<String, STentry> hmn = new HashMap<>();
							symTable.add(hmn);
							nestingLevel++;
						}
						LPAR
						(
							{
								int paroffset = 1;
							}
							fid = ID COLON fhty=hotype
							{
								// Gestione del primo parametro in input alla funzione
								parameters.add($fhty.ast);
								// Creo nodo ParNode
		            			ParNode fpar = new ParNode($fid.text, $fhty.ast);
		            			// Aggiungo il parametro alla funzione
		            			method.addPar(fpar);
		            			// Se e' di tipo funzionale
		            			if ($fhty.ast instanceof ArrowTypeNode)
		            				paroffset++;
		            			// Aggiungiamo la dichiarazione
		            			if (hmn.put($fid.text, new STentry(nestingLevel, $fhty.ast, paroffset++)) != null ) {
		            				System.out.println("Parameter id " + $fid.text + " at line " + $fid.line + " already declared");
		            				stErrors++;
		            			}
							}
								(
									COMMA id = ID COLON hty = hotype
									{
										// Gestione degli altri parametri a partire dal secondo in input alla funzione
				            			parameters.add($hty.ast);
				            			// Creo nodo ParNode
				            			ParNode par = new ParNode($id.text, $hty.ast);
				            			// Aggiungo il parametro al metodo
				            			method.addPar(par);
				            			// Se e' di tipo funzionale
				            			if ($hty.ast instanceof ArrowTypeNode)
				            				paroffset++;
				            			if (hmn.put($id.text, new STentry(nestingLevel, $hty.ast, paroffset++)) != null) {
				            				System.out.println("Parameter id " + $id.text + " at line " + $id.line + " already declared");
				            				stErrors++;
				            			}
									}
								)*
							)?
							RPAR
								(
									LET
									{
										int varoffset = -2;
									}
										(
											VAR i = ID COLON t = type ASS e = exp SEMIC
											{
												// Creiamo la struttura dati della variabile e la aggiungiamo
												VarNode v = new VarNode($i.text, $t.ast, $e.ast);
												method.addDec(v);

												// Aggiungiamo la dichiarazione al livello di nesting corrente
												Map<String, STentry> hmMethod = symTable.get(nestingLevel);
												
												// Controllo che non ci siano duplicati (dichiarazioni identiche)
												if (hmMethod.put($i.text, new STentry(nestingLevel, $t.ast, varoffset--)) != null) {
													System.out.println("Var id " + $i.text + " at line " + $i.line + " already declared");
													stErrors++;
												}
											}
										)+
										IN
								)?
								e = exp
								{
									method.addBody($e.ast);
				                	methodEntry.setType(new ArrowTypeNode(parameters, $ft.ast));
									
									// Rimuoviamo il contenuto dello scope corrente
									symTable.remove(nestingLevel--);	
								}
								SEMIC
					)*
					CRPAR 
					{
						symTable.remove(nestingLevel--);
						$classlist.add(clNode);
					}
	)+
; 

declist	returns [ArrayList<Node> astlist] :
	{
		$astlist = new ArrayList<Node>();
		int offset = nestingLevel == 0 ? globalOffset : -2;
	}
	// Vengono dichiarate variabili VAR o funzioni FUN
	(
		(
			VAR i = ID COLON ht = hotype ASS e = exp
				{
					// Creiamo la struttura dati della variabile e la aggiungiamo
					VarNode v = new VarNode($i.text, $ht.ast, $e.ast);
					$astlist.add(v);
					// Aggiungiamo la dichiarazione al livello di nesting corrente
					Map<String, STentry> hm = symTable.get(nestingLevel);
					
					// Controllo che non ci siano duplicati (dichiarazioni identiche)
					if (hm.put($i.text, new STentry(nestingLevel, $ht.ast, offset--)) != null) {
						System.out.println("Var id " + $i.text + " at line " + $i.line + " already declared");
						stErrors++;
					}
					// Se la dichiarazione e' di tipo funzionale si una un offset doppio
					if ($ht.ast instanceof ArrowTypeNode)
						offset--;
				}
			|  
            FUN i = ID COLON t = type
            	{
            		// Creiamo la struttura dati della funzione e la aggiungiamo
            		FunNode f = new FunNode($i.text, $t.ast);
            		$astlist.add(f);
            		// Aggiungiamo la dichiarazione al livello di nesting corrente
            		Map<String, STentry> hm = symTable.get(nestingLevel);
            		List<Node> parTypes = new ArrayList<Node>();
            		STentry entry = new STentry(nestingLevel, offset--);
            		if (hm.put($i.text, entry) != null) {
            			System.out.println("Fun id "+  $i.text + " at line " + $i.line + " already declared");
            			stErrors++;
            		}
            		// La dichiarazione e' di tipo funzionale quindi si ha un offset doppio (si sottrae di nuovo)
            		offset--;
            		// Creare la Symbol Table per questa funzione
            		nestingLevel++;
            		Map<String, STentry> hmn = new HashMap<String, STentry>();
            		symTable.add(hmn);
                }
            LPAR
            	{
            		int paroffset = 1;
            	}
            (
            	fid = ID COLON fhty = hotype
            		{
            			// Gestione del primo parametro in input alla funzione
            			parTypes.add($fhty.ast);
            			// Creo nodo ParNode
            			ParNode fpar = new ParNode($fid.text, $fhty.ast);
            			// Aggiungo il parametro alla funzione
            			f.addPar(fpar);
            			// Se e' di tipo funzionale
            			if ($fhty.ast instanceof ArrowTypeNode)
            				paroffset++;
            			// Aggiungiamo la dichiarazione
            			if (hmn.put($fid.text, new STentry(nestingLevel, $fhty.ast, paroffset++)) != null ) {
            				System.out.println("Parameter id " + $fid.text + " at line " + $fid.line + " already declared");
            				stErrors++;
            			}
            			
            		}
            	(
            	COMMA id = ID COLON hty = hotype
            		{
            			// Gestione degli altri parametri a partire dal secondo in input alla funzione
            			parTypes.add($hty.ast);
            			// Creo nodo ParNode
            			ParNode par = new ParNode($id.text, $hty.ast);
            			// Aggiungo il parametro alla funzione
            			f.addPar(par);
            			// Se e' di tipo funzionale
            			if ($hty.ast instanceof ArrowTypeNode)
            				paroffset++;
            			if (hmn.put($id.text, new STentry(nestingLevel, $hty.ast, paroffset++)) != null) {
            				System.out.println("Parameter id " + $id.text + " at line " + $id.line + " already declared");
            				stErrors++;
            			}
            			
            		}
            	)*
        	)?
           	RPAR
           		{
           			ArrowTypeNode type = new ArrowTypeNode(parTypes, $t.ast);
           			f.setSymType(type);
           			entry.setType(type);
           		}
           	(
           		LET d = declist IN
           			{
           				f.addDeclist($d.astlist);
           			}
           	)?
           	e = exp
           	{
           		f.addBody($e.ast);
           		// Rimuoviamo il contenuto dello scope corrente
           		symTable.remove(nestingLevel--);
           	}
      	)
      	SEMIC
    )+          
;
	
type returns [Node ast] :
	INT
		{
			$ast = new IntTypeNode();
		}
  	|
  	BOOL
  		{
  			$ast = new BoolTypeNode();
  		}
  	|
  	i = ID 
  		{ 
  			$ast = new RefTypeNode($i.text);
  		}
;

exp	returns [Node ast] :
	f = term
		{
			$ast = $f.ast;
		}
		(
			PLUS l = term
			{
				$ast = new PlusNode($ast, $l.ast);
			}
		|
			MINUS l = term
			{
				$ast = new MinusNode($ast, $l.ast);
			}
		|
			OR l = term
			{
				$ast = new OrNode($ast,$l.ast);
			}
 	    )*
 ;
 	
term returns [Node ast] :
	f = factor
		{
			$ast = $f.ast;
		}
	    (
	    	TIMES l = factor
	    	{
	    		$ast = new TimesNode($ast, $l.ast);
	    	}
	    |
	    	DIV l = factor
	    	{
	    		$ast = new DivNode($ast, $l.ast);
	    	}
	    |
	    	AND l = factor
	    	{
	    		$ast = new AndNode($ast, $l.ast);
	    	}
	    )*
;
	
factor returns [Node ast] :
	f = value
		{
			$ast= $f.ast;
		}
		(
			EQ l = value
			{
				$ast = new EqualNode($ast, $l.ast);
			}
		|
			GE l = value
			{
				$ast = new GreaterOrEqualNode($ast, $l.ast);
			}
		|
			LE l=value
			{
				$ast= new LessOrEqualNode($ast, $l.ast);
			}
	    )*
 ;	 	
 
value returns [Node ast] :
	n = INTEGER
	{
		$ast= new IntNode(Integer.parseInt($n.text));
	}  
	|
		TRUE
		{
			$ast = new BoolNode(true);
		}  
	|
		FALSE
		{
			$ast = new BoolNode(false);
		}
	|
		NULL
		{
			$ast = new EmptyNode();
		}
	|
		NEW id=ID 
		{
			STentry entry = symTable.get(0).get($id.text); // e' possibile dichiarare classi solo nell'ambiente globale
			if (entry != null && !classTable.containsKey($id.text)) {
				System.out.println("Class Id " + $id.text + " at line " + $id.line + " not declared");
				stErrors++;
			}
		}
		LPAR
		{
	       List<Node> arglist = new ArrayList<Node>();
	    }
			(e = exp 
				{
					arglist.add($e.ast);
				}
				(COMMA e = exp
					{
						arglist.add($e.ast);
					}
				)*
			)? RPAR
			{
				$ast = new NewNode($id.text, entry, arglist);
			}
	|
		LPAR e = exp RPAR
		{
			$ast = $e.ast;
		}  
	|
		IF x = exp THEN CLPAR y = exp CRPAR
			ELSE CLPAR z = exp CRPAR
				{
					$ast = new IfNode($x.ast, $y.ast, $z.ast);
				}	 
	|
		NOT LPAR e = exp RPAR
		{
			$ast = new NotNode($e.ast);
		}
	|
		PRINT LPAR e = exp RPAR
		{
			$ast = new PrintNode($e.ast);
		}
	|
		i = ID
		// Cerca la dichiarazione
		{
			int j = nestingLevel;
			STentry entry = null;
			while (j >= 0 && entry == null)
				entry = (symTable.get(j--)).get($i.text);
			if (entry == null) {
				System.out.println("Id " + $i.text + " at line " + $i.line + " not declared");
				stErrors++;
			}
			$ast = new IdNode($i.text, entry, nestingLevel);
		}
		(
			LPAR
				{
					List<Node> arglist = new ArrayList<Node>();
				}
				(
					a = exp
					{
						arglist.add($a.ast);
					}
					(
						COMMA a = exp
						{
							arglist.add($a.ast);
						}
					)*
	   	 		)?
	   	 		RPAR
	   	 		{
	   	 			$ast = new CallNode($i.text, entry, arglist, nestingLevel);
	   	 		}
	   	 	| 
	   	 		DOT i2 = ID LPAR 
	   	 		{
	   	 			List<Node> arglist = new ArrayList<Node>();
	   	 		}
	   	 		(e = exp 
	   	 			{
	   	 				arglist.add($e.ast);
	   	 			}
	   	 		(COMMA e = exp 
	   	 			{
	   	 				arglist.add($e.ast);
	   	 			}
	   	 		)*)? 
	   	 		{
	   	 			if (entry == null || !(entry.getType() instanceof RefTypeNode)) {
	   	 				System.out.println("Class Id " + $i.text + " at line " + $i.line + " not a ref type");
						stErrors++;
	   	 			}
	   	 			RefTypeNode classEntryType = (RefTypeNode) entry.getType();
					String classId = classEntryType.getId();
					
					// Contiene dichiarazioni interne alla classe con id classId
					Map<String, STentry> virtualTable = classTable.get(classId);
					
					STentry methodEntry = virtualTable.get($i2.text);
					
					if (methodEntry == null) {
						System.out.println("Method Id " + $i2.text + " at line " + $i2.line + " not declared");
						stErrors++;
					}
					
	   	 			$ast = new ClassCallNode(classId, $i2.text, entry, methodEntry, arglist, nestingLevel);
	   	 		} RPAR 
	   	 )?
;
 	
hotype returns [Node ast] :
	t = type
	{
		$ast = $t.ast;
	}
	|
	a = arrow
	{
		$ast = $a.ast;
	}
;
 	  
arrow returns [Node ast] :
	{
		List<Node> parTypes = new ArrayList<>();
	}
	LPAR
		(
			h = hotype 
			{
	     		parTypes.add($h.ast);
			}
			(
				COMMA h = hotype
		    	{
	     			parTypes.add($h.ast);
				}
			)*
		)?
		RPAR ARROW t = type
		{
			$ast = new ArrowTypeNode(parTypes, $t.ast);
  		}
;
  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS  	: '+' ;
MINUS   : '-' ;
TIMES   : '*' ;
DIV 	: '/' ;
LPAR	: '(' ;
RPAR	: ')' ;
CLPAR	: '{' ;
CRPAR	: '}' ;
SEMIC 	: ';' ;
COLON   : ':' ; 
COMMA	: ',' ;
DOT	    : '.' ;
OR	    : '||';
AND	    : '&&';
NOT	    : '!' ;
GE	    : '>=' ;
LE	    : '<=' ;
EQ	    : '==' ;	
ASS	    : '=' ;
TRUE	: 'true' ;
FALSE	: 'false' ;
IF	    : 'if' ;
THEN	: 'then';
ELSE	: 'else' ;
PRINT	: 'print' ;
LET     : 'let' ;	
IN      : 'in' ;	
VAR     : 'var' ;
FUN	    : 'fun' ; 
CLASS	: 'class' ; 
EXTENDS : 'extends' ;	
NEW 	: 'new' ;	
NULL    : 'null' ;	  
INT	    : 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ; 	
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*) ; 

ID  	: ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;

WHITESP	: ( '\t' | ' ' | '\r' | '\n' )+
			-> channel(HIDDEN) ;

COMMENT	: '/*' (.)*? '*/' -> channel(HIDDEN) ;
 
ERR		: .
			{ 
				System.out.println("Invalid char: "+ getText()); lexicalErrors++;
			} -> channel(HIDDEN); 
